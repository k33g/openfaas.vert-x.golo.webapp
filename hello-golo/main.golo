----
Made with ❤️ with Vert-x and Golo
Author: @k33g_org on Twitter
----
module webapp

import gololang.JSON
import gololang.Errors

# === Vert-x Imports ===
import io.vertx.core.Vertx
import io.vertx.core.http.HttpServer
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler

import demo.helpers
import demo.augmentations

let vertx = Vertx.vertx()

function main = |args| {

  let router = Router.router(vertx)
  router: route(): handler(BodyHandler.create())
  
  router: get("/hello", |context| -> context: json(["ola", "hi", "yo"]))

  router: get(
    "/divide/:a/:b", |context| -> trying({
      let a = Integer.parseInt(context: param("a"))
      let b = Integer.parseInt(context: param("b"))
      return a/b
    })
    : either(
        recover= |error| -> context: 😡(error: message())
      , mapping= |value| -> context: 😃(value)
    )
  )

  router: route("/*"): handler(StaticHandler.create("public"): setCachingEnabled(false))
  
  let port =  sys(): env("HTTP_PORT"): either(
      default= -> 8080
    , mapping= |value|-> Integer.parseInt(value)
  )

  vertx: createHttpServer(): requestHandler(router): listen(port, |asyncResult| {
    match {
      when asyncResult: failed() then log(): 😡("Houston? {0}", asyncResult: cause(): message())
      otherwise log(): 😃("🌍 Golo http server is listening on {0,number,#}", port)
    }    
  })
   
}

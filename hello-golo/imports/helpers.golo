module demo.helpers

import gololang.Errors

struct log = {
  _value
}

augment log {
  function 😃 = |this, txt, args...| -> println("😃 "+java.text.MessageFormat.format(txt, args))
  function 😡 = |this, txt, args...| -> println("😡 "+java.text.MessageFormat.format(txt, args))
  function ✅ = |this, txt, args...| -> println("✅ "+java.text.MessageFormat.format(txt, args))

}

struct sys = {
  _value
}

augment sys {
  function env = |this, variableName| -> Option(System.getenv(): get(variableName))
}

module demo.augmentations

import gololang.JSON

augment io.vertx.ext.web.Router {
  function get = |this, uri, handler| {
    return this: get(uri): handler(handler)
  }
}

augment io.vertx.core.http.HttpServerResponse {
  function json = |this, content| ->
    this: putHeader("content-type", "application/json;charset=UTF-8")
        : end(JSON.stringify(content))
}

augment io.vertx.ext.web.RoutingContext {

  function param = |this, param| -> this: request(): getParam(param)

  function json = |this, content| {
    this: response(): json(content)
  }
  function 😡 = |this, content| {
    this: response(): json(
      DynamicObject(): error("😡 " + content)
    )
  }
  function 😃 = |this, content| {
    this: response(): json(
      DynamicObject(): result("😃 " + content)
    )
  }
}